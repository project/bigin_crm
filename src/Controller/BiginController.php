<?php

namespace Drupal\bigin_crm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;
use Drupal\bigin_crm\BiginAuthService;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Contains \Drupal\bigin_crm\Controller\BiginController.
 */
class BiginController extends ControllerBase {

  /**
   * A request stack symfony instance.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Drupal messenger service.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The auth Service
   *
   * @var \Drupal\bigin_crm\BiginAuthService
   */
  protected $authService;

  /**
   * Configuration Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   A request stack symfony instance.
   * @param \Drupal\Core\Messenger\Messenger $messenger
   *   The Drupal messenger service.
   * @param \Drupal\bigin_crm\BiginAuthService $authService
   *   The auth Service.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   Configuration Factory.
   */

  public function __construct(
    RequestStack $requestStack,
    Messenger $messenger,
    BiginAuthService $authService,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->requestStack = $requestStack;
    $this->messenger = $messenger;
    $this->authService = $authService;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('messenger'),
      $container->get('bigin_crm.auth_service'),
      $container->get('config.factory'),
    );
  }

  /**
   * Initialize the settings form.
   * 
   * @return array
   *   Form settings.
   */
  public function initialize() {
    $token_form = $this->formBuilder()->getForm('Drupal\bigin_crm\Form\GetTokenForm');

    $config = $this->configFactory->get('bigin_crm.settings');
    // Redirect url
    $redirect = Url::fromRoute('bigin_crm.callback',[], ['absolute' => true])->toString();
    // Scopes for API requests to restrict clients from accessing unauthorized resources
    $scope = 'ZohoBigin.modules.contacts.CREATE,ZohoBigin.settings.layouts.READ,ZohoBigin.modules.deals.CREATE,ZohoBigin.users.READ';
    // The authorization request link
    $auth_url = $this->authService->url_account() . '/oauth/v2/auth?scope=' . $scope.'&client_id=' . $config->get('client_id') .
    '&response_type=code&access_type=offline&redirect_uri=' . $redirect . '&prompt=consent';
    $revoke_url = Url::fromRoute('bigin_crm.revoke',[])->toString();

    return [
      '#theme' => 'form_settings',
      '#form' => $token_form,
      '#auth_url' => $auth_url,
      '#revoke_url' => $this->authService->access_token() ? $revoke_url : null,
    ];
  }

  /**
   * Bigin returns the user here after user has authenticated.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to settings page.
   */
  public function callback() {
    $request = $this->requestStack->getCurrentRequest();
    if($request->query->has('code')) {
      // Get access tokens using authorization code
      $access = $this->authService->generate_access_token($request->query->get('code'));
      $this->messenger()->addMessage($access ? $this->t('Success') : $this->t('Failed'));
    }
    return $this->redirect('bigin_crm.admin_settings_form');
  }

  /**
   * Revoke the refresh token.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   A redirect response to settings page.
   */
  public function revoke() {
    $revoke_token = $this->authService->revoke_token();
    if ($revoke_token) {
      $this->messenger->addMessage($this->t("The token has been removed."));
    } else {
      $this->messenger->addError($this->t('We are having problems to revoke your token, try again.'));
    }
    return $this->redirect('bigin_crm.admin_settings_form');
  }
}
