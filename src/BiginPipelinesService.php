<?php

namespace Drupal\bigin_crm;

use Drupal\bigin_crm\BiginAuthService;
use Drupal\bigin_crm\Rest\RestClient;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * To add new deals to pipelines.
 */
class BiginPipelinesService {

  /**
   * The auth Service
   *
   * @var \Drupal\bigin_crm\BiginAuthService
   */
  protected $authService;

  /**
   * The rest client
   *
   * @var \Drupal\bigin_crm\Rest\RestClient
   */
  protected $client;

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a new Service object.
   *
   * @param \Drupal\bigin_crm\BiginAuthService $authService
   *   The auth Service.
   * @param \Drupal\bigin_crm\Rest\RestClient $client
   *   The rest client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory service object.
   */

  public function __construct(
    BiginAuthService $authService,
    RestClient $client,
    ConfigFactoryInterface $configFactory,
  ) {
    $this->authService = $authService;
    $this->client = $client;
    $this->configFactory = $configFactory;
  }

  /**
   * Add new deal.
   *
   * @param string $id
   *  The contact id.
   * @param string $name
   *  The contact name.
   * @param string $roles
   *  User role.
   *
   * @return string
   *  Api rest response.
   */

  public function create_deal($id, $name, $roles) {
    $config = $this->configFactory->get('bigin_crm.settings');
    $body['data'][0] = [
      'Deal_Name' => $config->get('deal_name'),
      'Stage' => $config->get('stage'),
      'Layout' => [
        'name' => $config->get('layout_name'),
        'id' => $config->get('layout'),
      ],
      'Contact_Name' => [
        'name' => $name,
        'id' => $id,
      ],
      'Closing_Date' => date('Y-m-d', strtotime($config->get('closing_date'))),
      'Pipeline' => $config->get('sub_pipeline'),
        'Owner' => [
          "id" => $config->get('deal_owner'),
        ],
      'Description' => $config->get('deal_description'),
    ];

    $url = $this->authService->url_api() . '/bigin/v1/Deals';
    $response = $this->client->api_call($url, [], $body, 'POST');
    return !empty($response->data) ? $response->data[0]->code : 'Error';
  }

  /**
   * To get list of the layouts.
   *
   * @return array
   *  Layouts list
   */
  public function get_layouts() {
    $path = $this->authService->url_api() . '/bigin/v1/settings/layouts';
    $params = [
      'module' => 'Deals',
    ];

    $response = $this->client->api_call($path, $params, []);
    return $response->layouts ?? [];
  }
}
