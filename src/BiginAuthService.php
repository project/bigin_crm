<?php

namespace Drupal\bigin_crm;

use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\bigin_crm\Rest\RestClient;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Component\Datetime\TimeInterface;

/**
 * Bigin OAuth2 authentication.
 */
class BiginAuthService {

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The rest client
   *
   * @var \Drupal\bigin_crm\Rest\RestClient
   */
  protected $client;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
  */
  protected $loggerFactory;

  /**
   * A date time instance.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * Constructs a new Service object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory service object.
   * @param \Drupal\bigin_crm\Rest\RestClient $client
   *   The rest client.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   Logger service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A date time instance.
   */

  public function __construct(
    ConfigFactoryInterface $configFactory,
    RestClient $client,
    Connection $database,
    LoggerChannelFactory $loggerFactory,
    TimeInterface $time,
  ) {
    $this->configFactory = $configFactory;
    $this->client = $client;
    $this->database = $database;
    $this->loggerFactory = $loggerFactory;
    $this->time = $time;
  }

  /**
   * Generate access token.
   *
   * @param string $code
   *   Authorization code.
   *
   * @return bool
   *   Returns true if the access token could be generated.
   */
  public function generate_access_token($code) {
    $redirectUri = Url::fromRoute('bigin_crm.callback',[], ['absolute' => TRUE])->toString();
    $params = [
      'client_id' => $this->setting('client_id'),
      'client_secret' => $this->setting('client_secret'),
      'code' => $code,
      'grant_type' => 'authorization_code',
      'redirect_uri' => $redirectUri
    ];

    $url = $this->url_account() . '/oauth/v2/token';
    $response = $this->client->api_call($url, $params, [], 'POST');
    // save token
    if(!empty($response->access_token) && !empty($response->refresh_token)) {
      // Save token to database
      $this->database->insert('bigin_crm_token')
        ->fields(['access_token', 'refresh_token', 'created'])
        ->values([
          $response->access_token,
          $response->refresh_token,
          $this->time->getCurrentTime()
        ])->execute();

      $this->loggerFactory->get('bigin')->info(t("Successfully generated token"));
    }
    return empty($response->access_token) ? FALSE : TRUE;
  }

  /**
   * Get access token.
   *
   * @return string
   *   Token.
   */
  public function access_token() {
    $result = $this->database->select('bigin_crm_token', 't')
    ->fields('t', ['access_token'])
    ->orderBy('id', 'DESC')
    ->execute()->fetchAssoc();
    
    return $result['access_token'] ?? '';
  }

  /**
   * Get new access token using refresh token.
   *
   * @return bool
   *   Returns TRUE if the access token could be generated.
   */
  public function refresh_token() {
    $token = $this->database->select('bigin_crm_token', 't')
    ->fields('t', ['id', 'refresh_token'])
    ->condition('access_token', $this->access_token())
    ->execute()->fetchAssoc();

    // Request Parameters
    $params = [
      'client_id' => $this->setting('client_id'),
      'client_secret' => $this->setting('client_secret'),
      'refresh_token' => $token['refresh_token'],
      'grant_type' => 'refresh_token',
    ];

    $url = $this->url_account() . '/oauth/v2/token';
    $response = $this->client->api_call($url, $params, [], 'POST');

    if(!empty($response->access_token)) {
      // Update token in database
      $this->database->update('bigin_crm_token')
        ->fields([
          'access_token' => $response->access_token,
          'created' => $this->time->getCurrentTime(),
        ])
        ->condition('id', $token['id'])
        ->execute();
      }
      $this->loggerFactory->get('bigin')->info(t("Token updated successfully"));
    return empty($response->access_token) ? FALSE : TRUE;
  }

  /**
   * Revoke refresh tokens.
   *
   * @return bool
   * Returns the response from the api
   */
  public function revoke_token() {
    $token = $this->access_token();
    $params = [
      'token' => $token,
    ];
    $url = $this->url_account() . '/oauth/v2/token/revoke';
    $response = $this->client->api_call($url, $params, [], 'POST');

    if(!empty($response->status) && $response->status == 'success') {
      $this->database->delete('bigin_crm_token')
        ->condition('access_token', $token)
        ->execute();
      
      $this->loggerFactory->get('bigin')->info(t("The token has been removed"));
      return TRUE;
    } else {
      $this->refresh_token();
      return FALSE;
    }
  }

  /**
   * Get domain-specific accounts URL to generate access and refresh token.
   *
   * @return string
   *   The URL.
   */
  public function url_account() {
    $accountsArray = [
      'com' => 'https://accounts.zoho.com',
      'eu' => 'https://accounts.zoho.eu',
      'cn' => 'https://accounts.zoho.com.cn',
      'in' => 'https://accounts.zoho.in',
    ];
    return $accountsArray[$this->setting('domain')];
  }

  /**
   * Get domain specific accounts url for rest api.
   *
   * @return string
   *   The URL.
   */
  public function url_api() {
    $apiArray = [
      'com' => 'https://www.zohoapis.com',
      'eu' => 'https://www.zohoapis.eu',
      'cn' => 'https://www.zohoapis.com.cn',
      'in' => 'https://www.zohoapis.in',
    ];
    return $apiArray[$this->setting('domain')];
  }

  /**
   * Access the settings of this module.
   *
   * @param string $key
   *   The key of the configuration.
   *
   * @return string
   *   The value of the configuration item requested.
   */
  protected function setting($key) {
    return $this->configFactory->get('bigin_crm.settings')->get($key);
  }
}
