<?php

namespace Drupal\bigin_crm;

use Drupal\bigin_crm\BiginAuthService;
use Drupal\bigin_crm\Rest\RestClient;
use Drupal\bigin_crm\BiginPipelinesService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * To add new entities to the contacts module.
 */
class BiginContactsService {

  /**
   * The auth Service.
   *
   * @var \Drupal\bigin_crm\BiginAuthService
   */
  protected $authService;

  /**
   * The rest client.
   *
   * @var \Drupal\bigin_crm\Rest\RestClient
   */
  protected $client;

  /**
   * The pipeline Service.
   *
   * @var \Drupal\bigin_crm\BiginPipelinesService
   */
  protected $pipelineService;

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Drupal Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;


  /**
   * Constructs a new Service object.
   *
   * @param \Drupal\bigin_crm\BiginAuthService $authService
   *   The auth Service.
   * @param \Drupal\bigin_crm\Rest\RestClient $client
   *   The rest client.
   * @param \Drupal\bigin_crm\BiginPipelinesService $pipelineService
   *   The pipeline Service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory service object.
   */

  public function __construct(
    BiginAuthService $authService,
    RestClient $client,
    BiginPipelinesService $pipelineService,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->authService = $authService;
    $this->client = $client;
    $this->pipelineService = $pipelineService;
    $this->configFactory = $configFactory;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Add new contact to Bigin crm.
   *
   * @param array $user
   * User data.
   *
   * @return string
   * Api rest response
   */
  public function create($user) {
    $config = $this->configFactory->get('bigin_crm.settings');
    // User data
    $body['data'][0] = [
      'Last_Name' => $user['name'],
      'First_Name' => '',
      'Email' => $user['email'],
      'Owner' => [
        'id' => $config->get('deal_owner')
      ]
    ];
    
    $url = $this->authService->url_api() . '/bigin/v1/Contacts';
    $response = $this->client->api_call($url, [], $body, 'POST');

    if (!empty($response->data) && $response->data[0]->code == 'SUCCESS') {
      // Add new deal to pipelines
      $res = $this->pipelineService->create_deal(
        $response->data[0]->details->id,
        $user['name'],
        $user['roles'],
      );
      $this->loggerFactory->get('bigin')->info(t('A contact has been created in Bigin'));
      return $res;
    } else {
      $this->loggerFactory->get('bigin')->error(t('Error: @message', [
        '@message' => $response->data[0]->message ?? t('Error creating a contact')
      ]));
      return '';
    }
  }

  /**
   * Get list users.
   *
   * @return array
   * list users
   */
  public function get_users() {
    $path = $this->authService->url_api() . '/bigin/v1/users';
    $params = [
      'type' => 'AllUsers',
    ];

    $response = $this->client->api_call($path, $params, []);
    return $response->users ?? [];
  }

}
