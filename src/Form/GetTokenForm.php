<?php

namespace Drupal\bigin_crm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 *  Bigin CRM integration Settings form.
 */
class GetTokenForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
  ) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'bigin_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames()
  {
    return [
      'bigin_crm.settings',
    ];
  }

  /**
   * Defines the settings form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bigin_crm.settings');
    $form['client_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('client_id'),
      '#required' => TRUE,
      '#description' => $this->t('Copy the Client ID here.'),
    ];

    $form['client_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('client_secret'),
      '#required' => TRUE,
      '#description' => $this->t('Copy the Client Secret here.'),
    ];

    $form['domain'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose your Domain'),
      '#default_value' => $config->get('domain'),
      '#options' => [
        'com' => '.COM (Default)',
        'eu' => '.EU',
        'cn' => '.CN',
        'in' => '.IN',
      ],
      '#required' => TRUE,
      '#description' => $this->t('Domains to access your application'),
    ];

    $form['authorized_redirect_url'] = [
      '#type' => 'textfield',
      '#disabled' => TRUE,
      '#title' => $this->t('Authorized redirect URIs'),
      '#description' => $this->t('Copy this value to <em>Authorized Redirect URIs</em> field of your Zoho Api console.'),
      '#default_value' => Url::fromRoute('bigin_crm.callback')->setAbsolute()->toString(),
    ];

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select roles'),
      '#default_value' => $config->get('roles'),
      '#options' => $this->getRoles(),
      '#required' => TRUE,
      '#description' => $this->t('Only users with the selected roles will be registered in Bigin crm'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_keys = [
      'client_id', 'client_secret', 'domain', 'roles',
    ];
    $crm_config = $this->config('bigin_crm.settings');
    foreach ($config_keys as $config_key) {
      if ($form_state->hasValue($config_key)) {

        if ($config_key == 'roles') {
          $crm_config->set($config_key, array_filter($form_state->getValue(
            $config_key
          )));
        }
        else {
          $crm_config->set($config_key, $form_state->getValue($config_key));
        }
      }
    }
    $crm_config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Retrieves from current installation all Drupal User Roles created.
   *
   * @return array
   *  Role list
   */
  private function getRoles() {
    $roles = [];
    $all_roles = $this->entityTypeManager->getStorage('user_role')
      ->loadMultiple();
    foreach ($all_roles as $item) {
      $roles[$item->id()] = $item->label();
    }
    return $roles;
  }
}
