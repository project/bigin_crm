<?php

namespace Drupal\bigin_crm\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\bigin_crm\BiginPipelinesService;
use Drupal\bigin_crm\BiginContactsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Pipeline configuration form.
 */
class SettingsPipelinesForm extends ConfigFormBase {

  /**
   * The pipeline Service
   *
   * @var \Drupal\bigin_crm\BiginPipelinesService
   */
  protected $pipelineService;

  /**
   * The contacts Service
   *
   * @var \Drupal\bigin_crm\BiginContactsService
   */
  protected $contactsService;

  /**
   * @param \Drupal\bigin_crm\BiginPipelinesService $pipelineService
   *   The pipeline Service.
   * @param \Drupal\bigin_crm\BiginContactsService $contactsService
   *   The contacts Service.
   */

  public function __construct(
    BiginPipelinesService $pipelineService,
    BiginContactsService $contactsService,
  ) {
    $this->pipelineService = $pipelineService;
    $this->contactsService = $contactsService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('bigin_crm.pipelines_service'),
      $container->get('bigin_crm.contacts_service'),
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'bigin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames()
  {
    return [
      'bigin_crm.settings',
    ];
  }

  /**
   * Defines the pipeline configuration form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('bigin_crm.settings');

    $form['pipelines'] = [
      '#type' => 'details',
      '#title' => $this->t('Module pipelines settings'),
      '#open' => TRUE,
    ];

    $form['pipelines']['layout'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $config->get('layout'),
      '#options' => $this->get_layouts(),
      '#title' => 'Pipeline',
      '#description' => $this->t('Select pipeline.')
    ];

    $form['pipelines']['sub_pipeline'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('sub_pipeline'),
      '#title' => 'Sub-Pipeline',
      '#description' => $this->t('Enter Sub-Pipeline name.')
    ];

    $form['pipelines']['stage'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('stage'),
      '#title' => 'Stage',
      '#description' => $this->t('Enter open stages name.')
    ];

    $form['pipelines']['closing_date'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('closing_date'),
      '#title' => 'Closing Date',
      '#description' => $this->t('Enter closing date, example: +5 day.')
    ];

    $form['pipelines']['deal_owner'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#default_value' => $config->get('deal_owner'),
      '#options' => $this->get_users(),
      '#title' => 'Owner',
      '#description' => $this->t('Select the owner to assign to the deal and contact.')
    ];

    $form['pipelines']['deal_name'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => $config->get('deal_name'),
      '#title' => 'Deal name',
      '#description' => $this->t('Enter deal name.')
    ];

    $form['pipelines']['deal_description'] = [
      '#type' => 'textfield',
      '#required' => FALSE,
      '#default_value' => $config->get('deal_description'),
      '#title' => 'Deal description',
      '#description' => $this->t('Enter deal description.')
    ];
    
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $config_keys = [
     'layout', 'sub_pipeline', 'stage', 'closing_date', 'deal_name', 'deal_owner', 'deal_description'
    ];
    $crm_config = $this->config('bigin_crm.settings');
    foreach ($config_keys as $config_key) {
      if ($form_state->hasValue($config_key)) {
        $crm_config->set($config_key, $form_state->getValue($config_key));
      }
    }
    $crm_config->set('layout_name', $form['pipelines']['layout']['#options'][$form_state->getValue('layout')]);
    $crm_config->save();
    parent::submitForm($form, $form_state);
  }
  /**
   * Get list layouts from bigin crm.
   *
   * @return array
   *  Pipeline list
   */
  private function get_layouts()
  {
    $data = [];
    $pipelines = $this->pipelineService->get_layouts();

    foreach ($pipelines as $item) {
      $data[$item->id] = $item->name;
    }
    return $data;
  }

  /**
   * Get list users from bigin crm.
   *
   * @return array
   *  User list
   */
  private function get_users()
  {
    $data = [];
    $users = $this->contactsService->get_users();

    foreach ($users as $item) {
      $data[$item->id] = $item->first_name;
    }
    return $data;
  }
}
