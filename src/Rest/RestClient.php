<?php

namespace Drupal\bigin_crm\Rest;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Component\Utility\NestedArray;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\GuzzleException;
use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * Service to communicate with the Bigin API.
 */
class RestClient {

  /**
   * GuzzleHttp client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Response object.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  public $response;

  /**
   * Config Factory service object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
  */
  protected $loggerFactory;

  /**
   * Constructor which initializes the consumer.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The GuzzleHttp Client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $loggerFactory
   *   Logger service.
   */

  public function __construct(
    ClientInterface $httpClient,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $loggerFactory,
  ) {
    $this->configFactory = $configFactory;
    $this->httpClient = $httpClient;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Call bigin api
   *
   * @param string $url
   *   Resource URL.
   * @param array $params
   *   Parameters to provide.
   * @param array $body
   *   The request body.
   * @param string $method
   *   Method to initiate the call, such as GET or POST.  Defaults to GET.
   *
   */
  public function api_call($url, $params = [], $body = [], $method = 'GET') {
    // validate only when the url is https://www.zohoapis.com not https://accounts.zoho.com
    // validate if exist access token
    if (strpos($url, \Drupal::service('bigin_crm.auth_service')->url_api()) === 0 && !\Drupal::service('bigin_crm.auth_service')->access_token()) {
      $this->loggerFactory->get('bigin')->error("Invalid token");
      return [];
    }
    try {
      // Create a request
      $this->response = $this->request_http($url, $params, $body, $method);

    } catch (RequestException $e) {
      $this->response = $e->getResponse();
      $this->loggerFactory->get('bigin')->error(t("Exception message: @error", [
        '@error' => $e->getMessage()
      ]));
    }
    // Validate status
    if ($this->response->getStatusCode() == 401) {
      // If the status code is (401 Unauthorized) then refresh token
      \Drupal::service('bigin_crm.auth_service')->refresh_token();
      try {
        $this->response = $this->request_http($url, $params, $body, $method);
      } catch (RequestException $e) {
        $this->response = $e->getResponse();
        $this->loggerFactory->get('bigin')->error(t("Exception message: @error", [
          '@error' => $e->getMessage()
        ]));
      }
    }

    return $this->response_http($this->response);
  }

  /**
   * Create a request HTTP.
   *
   * @param string $url
   *   Resource URL.
   * @param array|string $params
   *   Parameters to provide.
   * @param array|string $body
   *   The request body.
   * @param string $method
   *   Method to initiate the call, such as GET or POST.  Defaults to GET.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response object.
   */
  protected function request_http($url, $params, $body, $method) {
    // define headers
    $headers['Content-type'] = 'application/json';
    $headers['Authorization'] = 'Zoho-oauthtoken ' . \Drupal::service('bigin_crm.auth_service')->access_token();
    $data = null;
    if (!empty($body)) {
      $data = is_array($body) ? json_encode($body) : $body;
    }
    $args = NestedArray::mergeDeep(['headers' => $headers, 'query' => $params, 'body' => $data]);

    return $this->httpClient->$method($url, $args);
  }

  /**
   * RestResponse
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Object.
   * 
   * @return array
   *   Response.
   */
  protected function response_http($response) {
    $responseBody = $response->getBody()->getContents();
    if (empty($responseBody)) {
      return [];
    } else {
      return json_decode($responseBody);
    }
  }

}
