# Bigin CRM integration

## Introduction

The main purpouse of this module is add all your Drupal new users accounts like
prospects in Bigin CRM, the new kid on the block of Zoho familiy. Uses the Bigin
Rest APIs to provide integration.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. See
Installing Modules for more details.

## Configuration

In Zoho console develop

- **Open the API console**

  Go to Zoho API Console https://api-console.zoho.com/.

- **Select a client type**

  If you are registering an application for the first time, click GET STARTED.

  If you already registered an application and want to register a new one, then
  click + ADD CLIENT.

  From the Choose the Client Type window, select Server-based Applications.

- **Fill out client details**

  Based on the selected client type, you must provide the following details and
  then click CREATE:

  **Client Name:** Type the name of the application.

  **Homepage URL:** Provide the home page URL of your application.

  **Authorized Redirect URIs:** Provide a webpage URL of your application to
  which the accounts URL redirects you with an authorization code after the
  user's successful validation.

- **(Optional) Configure multi-dc for the client**

  You can now set up the multi-dc configuration for your application.
  The multi-dc configuration allows you to control the users from different
  domains to access your application.

Once the registration of your application is complete, you will receive the
following credentials that are used to identify your registered application:

**Client ID:** A unique identifier that contains the registration information of
an application. The authorization server identifies the application using this
client identifier.

**Client Secret:** A unique key that helps authenticate an application with the
authorization server. The client secret is privy to the application and
authorization server and must be kept safe.

In Drupal

- Return to Configuration » Bigin
- Enter the **Client ID**.
- Enter the **Client Secret**.
- Choose your **Domain**.
- Select roles
- Click Save configuration.

After saving the configuration data you can now generate your token:

Click **Get authorization button**

After get the token, go to **Bigin settings** and configure the data pipelines
so you can create a new deal.

## How it works

After insert a new user in drupal it is added as prospects in Bigin CRM and is
assigned to a new deal.
